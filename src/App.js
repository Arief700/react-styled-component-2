import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Hero from "./Components/Hero";
import Products from "./Components/Products";
import { GlobalStyle } from "./GlobalStyle";
import { ProductDataTwo,ProductData } from './Components/Products/data'
import Feature from "./Components/Feature";
import Footer from "./Components/Footer";

const App = () => {
  return (
    <Router>
      <GlobalStyle />
      <Hero />
      <Products heading="Choose your Favorite" data={ProductData} />
      <Feature/>
      <Products heading="Sweet Treats for you" data={ProductDataTwo} />
      <Footer/>
    </Router>
  );
};

export default App;
